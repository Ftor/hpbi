const express = require('express'),
    path = require('path'),
    hbs = require('express-handlebars'),
    bodyParser = require('body-parser');

const app = express();

app.set('port',process.env.PORT || 3000);

app.disable('x-powered-by');

app.engine('hbs', hbs({
    extname: 'hbs',
    defaultLayout: 'main'
}));
app.set('view engine', 'hbs');

app.use(express.static(path.join(__dirname, '/public')));

app.use(bodyParser.urlencoded({ extended: true }));



app.use(require('./routes'));

app.use((req, res, next) =>{
    res.status(404).render('404');

});

app.use((err, req, res, next)=> {
    res.status(err.status || 500).render('500');
});

app.listen(app.get('port'),()=>{
    console.log(`Server work in ${app.get('env')} mode. Server listening on http://localhost:${app.get('port')};`);
});