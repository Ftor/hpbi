addEventListener("load", ()=>{
    document.querySelector('button').addEventListener('click',start);
    document.body.style.backgroundColor = "#E5FFCC";
    document.querySelector("#mainField").style.color = "black";
});

function playHb() {
    let audio = new Audio(`./audio/hb.mp3`);
    audio.volume = 0.25;
    audio.play();
}

function mainLogic() {
    const app = document.getElementById('mainField');

    const typewriter = new Typewriter(app, {
        loop: false,
        delay: 75,
    });

    typewriter
        .pauseFor(1500)
        .typeString('Привет!')
        .pauseFor(1000)
        .deleteAll()
        .callFunction(playHb)
        .typeString('Илюха, дружище! От всей души поздравляю тебя!')
        .pauseFor(1000)
        .deleteAll()
        .typeString('Радуйся каждый день, наслаждайся мелочами и стремись к своим мечтам!')
        .pauseFor(1000)
        .deleteAll()
        .typeString('<strong>Поздравляю тебя с <strong><span style="color: #FF3399;">Д</span><span style="color: #3399FF;">Н</span><span style="color: #9933FF;">Ё</span><span style="color: #009900;">М</span> <span style="color: #EBDD64;">Р</span><span style="color: #FF3333;">О</span><span style="color: #FF3399;">Ж</span><span style="color: #3399FF;">Д</span><span style="color: #9933FF;">Е</span><span style="color: #009900;">Н</span><span style="color: #EBDD64;">И</span><span style="color: #FF3399;">Я</span> <span style="color: #3399FF;">!</span></span></strong></strong>')
        .start();
}

function start(e){

    e.target.style.display = 'none';
    document.querySelector(".intro").style.display = "inline-block";
    mainLogic();
}


