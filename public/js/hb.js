addEventListener("load", ()=>{
    document.querySelector('button').addEventListener('click',start)
});

function hello() {
    anime.timeline({loop: false})
        .add({
            targets: '.ml5 .line',
            opacity: [0.5,1],
            scaleX: [0, 1],
            easing: "easeInOutExpo",
            duration: 700
        }).add({
        targets: '.ml5 .line',
        duration: 600,
        easing: "easeOutExpo",
        translateY: (el, i) => (-0.625 + 0.625*2*i) + "em"
    }).add({
        targets: '.ml5 .ampersand',
        opacity: [0,1],
        scaleY: [0.5, 1],
        easing: "easeOutExpo",
        duration: 600,
        offset: '-=600'
    }).add({
        targets: '.ml5 .letters-left',
        opacity: [0,1],
        translateX: ["0.5em", 0],
        easing: "easeOutExpo",
        duration: 600,
        offset: '-=300'
    }).add({
        targets: '.ml5 .letters-right',
        opacity: [0,1],
        translateX: ["-0.5em", 0],
        easing: "easeOutExpo",
        duration: 600,
        offset: '-=600'
    }).add({
        targets: '.ml5',
        opacity: 0,
        duration: 1000,
        easing: "easeOutExpo",
        delay: 2000
    });


}

function playHp() {
   let audio = new Audio(`./audio/hp.mp3`);
    audio.play();
}
function playGren() {
    let audio = new Audio(`./audio/gren.mp3`);
    audio.volume = 0.15;
    audio.play();
}
function playHb() {
    let audio = new Audio(`./audio/hb.mp3`);
    audio.volume = 0.25;
    audio.play();
}

function mainLogic() {
    document.querySelector(".text-wrapper").style.display ='none';
    document.getElementById('mainField').style.display = 'inline-block';
    document.querySelector(".ml5").style.opacity =100;
    const app = document.getElementById('mainField');

    const typewriter = new Typewriter(app, {
        loop: false,
        delay: 75,
    });

    typewriter
        .pauseFor(2500)
        .typeString('Привет!')
        .pauseFor(1000)
        .deleteAll()
        .typeString('Сейчас я буду поздравлять <strong>ТЕБЯ</strong> с Днём Рождения 🎂')
        .pauseFor(2000)
        .deleteAll()
        .typeString('Но есть <strong><span style="color: red;">правила</span></strong>📕')
        .pauseFor(1000)
        .deleteAll()
        .typeString('<strong>Первое правило</strong> Бойцовского клуба никому не рас')
        .pauseFor(300)
        .deleteChars(32)
        .typeString(': читай всё что тут написано')
        .pauseFor(1000)
        .deleteAll()
        .typeString('Больше правил нет')
        .pauseFor(1000)
        .deleteAll()
        .typeString('Приступим к поздравлению')
        .pauseFor(1000)
        .deleteAll()
        .typeString('Илюха')
        .pauseFor(1000)
        .typeString('!')
        .pauseFor(1000)
        .typeString('!!!!!')
        .pauseFor(1000)
        .deleteAll()
        .typeString('Так не пойдёт, чего-то не хватает... 🤔')
        .pauseFor(2000)
        .deleteAll()
        .typeString('Придумал 💡')
        .pauseFor(1000)
        .deleteAll()
        .pauseFor(500)
        .callFunction(() => {
            document.body.style.backgroundColor = 'black';
        })
        .pauseFor(3000)
        .callFunction(() => {
            document.querySelector("#mainField").style.color = "white";
        })
        .typeString('Ты ещё тут?')
        .pauseFor(1000)
        .deleteAll()
        .typeString('Видимо это была не та кнопка 😑')
        .pauseFor(1000)
        .deleteAll()
        .typeString('Сейчас всё исправим 🛠')
        .pauseFor(1000)
        .deleteAll()
        .callFunction(() => {
            document.body.style.backgroundImage = "url('./img/bg.jpg')";
            document.querySelector("#mainField").style.color = "black";
        })
        .pauseFor(1000)
        .typeString('Другое дело. Но чего-то ещё не хватает...')
        .pauseFor(1000)
        .deleteAll()
        .typeString('Точно, нужна <strong>музыка 🎶</strong>')
        .pauseFor(1000)
        .deleteAll()
        .typeString('Сейчас включу что-нибудь...')
        .pauseFor(1000)
        .callFunction(playHp)
        .pauseFor(3000)
        .deleteAll()
        .typeString('')
        .typeString('Нет, это не то что нужно')
        .pauseFor(1000)
        .deleteAll()
        .typeString('Может вот это?')
        .callFunction(playGren)
        .pauseFor(3000)
        .deleteAll()
        .typeString('Снова не то...')
        .pauseFor(2000)
        .deleteAll()
        .callFunction(playHb)
        .typeString('То что нужно!')
        .pauseFor(1000)
        .deleteAll()
        .typeString('Начнём!')
        .pauseFor(1000)
        .deleteAll()
        .typeString('Илюха! Ты крутой и <strong>cпасибо</strong> тебе за это!')
        .pauseFor(1000)
        .deleteAll()
        .typeString('Шлю тебе заряд позитива! И отличного тебе дня!')
        .pauseFor(1000)
        .deleteAll()
        .typeString('<strong>Дружище! Поздравляю тебя <strong><span style="color: #13bc13;">с ДНЁМ РОЖДЕНИЯ!</span></strong></strong>')
        .callFunction(() => {
            document.body.style.backgroundImage = "url('./img/bg.jpg')";
            document.querySelector("#mainField").style.color = "black";
        })
        .start();
}

function start(e){
    e.target.style.display = 'none';
    document.querySelector(".intro").style.display = "inline-block";
    hello();
    setTimeout(mainLogic,5000);
}


